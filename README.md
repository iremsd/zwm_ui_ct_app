## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Wed Mar 17 2021 09:35:45 GMT+0300 (GMT+03:00)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.4|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.34.229.3:8000/sap/opu/odata/sap/ZWM_TRANSFER_SRV
|**Module Name**<br>zwm_ui_ct_app|
|**Application Title**<br>App Title|
|**Namespace**<br>com.arete|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## zwm_ui_ct_app

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


