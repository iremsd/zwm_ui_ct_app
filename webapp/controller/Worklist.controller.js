sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/MessageBox"
        ], function(BaseController, JSONModel, formatter, Filter, FilterOperator, Dialog, DialogType, MessageToast, Button, ButtonType, MessageBox) {
	"use strict";
        
	return BaseController.extend("com.arete.zwmuictapp.controller.Worklist", {
        
	    formatter: formatter,
        
	    onInit: function() {
	        var that = this,
	         demLogoImage = jQuery.sap.getModulePath("com.arete.zwmuictapp");
	        var oViewModel = new JSONModel({
		worklistTableTitle: this.getResourceBundle().getText("worklistTableTitle"),
		worklistTableTitleBarkod: this.getResourceBundle().getText("worklistTableTitleBarkod"),
		tableNoDataText: this.getResourceBundle().getText("tableNoDataText"),
		DepoAdres: "",
		DemLogo: demLogoImage,
		BarkodTable: [],
		StokTable: [],
		TBStokTable: [],
		TotalMenge: "",
		CountMenge: "",
		Ivnum: "",
		Lgnum: "",
		BarkodTip: "",
		Barkod: "",
		TbBarkod: "",
		TbAdet: "",
		fragment: "",
		StatusTrue: "Sayıldı",
		StatusFalse: "Sayılmadı",
		ToplamMenge: 0,
		KontrolBayrak: "false",
		TapKontrol: "false",
		EklenenStokTabArr: [],
		EklenenTabBarkodKontrol: [],
		StokTablosuGrupsuz: [],
		Countmengetap: "",
		AnaBarkodStock: [],
		Filtrelenenler: [],
		ExpandStok: false,
		ExpandBarkod: false,
        
	        });
	        this.setModel(oViewModel, "worklistView");
	        this.getOwnerComponent().getService("ShellUIService").then(function(oShellService) {
        
		oShellService.setBackNavigation(function() {
        
			debugger;
		    var oViewModel=that.getModel("worklistView"),
		    	oDataModel = that.getModel(),
		        depoInput = oViewModel.getProperty("/DepoAdres"),
		        depoIvnum = oViewModel.getProperty("/Ivnum"),
		        depoLgnum = oViewModel.getProperty("/Lgnum");
		        debugger;
		    if (!depoInput) {
        debugger;
		        oDataModel.callFunction("/CountUnlock", {
			urlParameters: {
			    Lgnum: depoLgnum,
			    Lgpla: depoInput,
			    Ivnum: depoIvnum,
			},
			success: function() {
        
			    that.onNavBack();
			},
			error: function() {
        
			    alert("noo");
			},
		        });
		    }
		});
	        });
        
	    },
	    onNavBack: function() {
	        var oHistory = History.getInstance(),
		sPreviousHash = oHistory.getPreviousHash();
        
	        if (sPreviousHash !== undefined) {
		window.history.go(-1);
	        } else {
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo("overview", true);
	        }
	    },
        
        
	    onDepoAlaniAdres: function(oEvent) {
        
	        var that = this,
		oViewModel = this.getModel("worklistView"),
		oDataModel = this.getModel(),
		depoadresInput = oViewModel.getProperty("/DepoAdres");
	        oDataModel.read(
		"/CountHeaderSet(Lgpla='" + depoadresInput + "')", {
		    urlParameters: {
		        "$expand": "BarcodeTab"
		    },
		    success: function(data) {
        
		        var tempArr = data;
		        if (tempArr.Istat === "Z") {
			var sText = "Adres sayımı tamamlanmış. Tekrar saymak istiyor musunuz?";
			MessageBox.confirm(sText, {
			    title: "Teyit",
			    initialFocus: sap.m.MessageBox.Action.CANCEL,
			    onClose: function(sButton) {
			        if (sButton === MessageBox.Action.OK) {
        
				oViewModel.setProperty("/BarkodTable", tempArr.BarcodeTab.results);
				oViewModel.setProperty("/TotalMenge", tempArr.TotalMenge);
				oViewModel.setProperty("/CountMenge", tempArr.CountMenge);
				oViewModel.setProperty("/Ivnum", tempArr.Ivnum);
				oViewModel.setProperty("/Lgnum", tempArr.Lgnum);
				oViewModel.setProperty("/ExpandBarkod", true);
			        } else if (sButton === MessageBox.Action.CANCEL) {
				oViewModel.setProperty("/DepoAdres", "");
				tempArr = "";
			        } else if (sButton === "Custom Button") {
        
			        };
			    }
			});
		        } else {
			var tempArr = data;
			oViewModel.setProperty("/BarkodTable", tempArr.BarcodeTab.results);
			oViewModel.setProperty("/TotalMenge", tempArr.TotalMenge);
			oViewModel.setProperty("/CountMenge", tempArr.CountMenge);
			oViewModel.setProperty("/Ivnum", tempArr.Ivnum);
			oViewModel.setProperty("/Lgnum", tempArr.Lgnum);
			oViewModel.setProperty("/ExpandBarkod", true);
        
		        }
        
		    },
		    error: function() {
        
        
		    },
		}
	        );
	    },
	    onPressDepoAdresiClear: function(oEvent) {
	        var oDataModel = this.getModel(),
		that = this,
		oViewModel = this.getModel("worklistView"),
		depoInput = oViewModel.getProperty("/DepoAdres"),
		depoIvnum = oViewModel.getProperty("/Ivnum"),
		depoLgnum = oViewModel.getProperty("/Lgnum");
	        oDataModel.callFunction("/CountUnlock", {
		urlParameters: {
		    Lgnum: depoLgnum,
		    Lgpla: depoInput,
		    Ivnum: depoIvnum,
		},
		success: function() {
		    that.onMethodClear();
        
        
        
		},
		error: function() {
        
		    alert("noo");
		},
	        });
        
	    },
	    onBarkodGiris: function(oEvent) {
        
	        var oDataModel = this.getModel(),
		oViewModel = this.getModel("worklistView"),
		girilenBarkod = oViewModel.getProperty("/Barkod"),
		array = oViewModel.getProperty("/BarkodTable"),
		grupsuzStok = oViewModel.getProperty("/StokTablosuGrupsuz");
        
	        for (var i = 0; i < array.length; i++) {
		if (array[i].Lenum === girilenBarkod && array[i].Status === true) {
        
        
		    var sText = "Okutulan barkod daha önce sayıldı. Tekrar saymak istiyor musunuz?";
		    MessageBox.confirm(sText, {
		        title: "Teyit",
		        initialFocus: sap.m.MessageBox.Action.CANCEL,
		        onClose: function(sButton) {
			if (sButton === MessageBox.Action.OK) {
			    var array = oViewModel.getProperty("/BarkodTable");
			    for (var i = 0; i < array.length; i++) {
			        if (array[i].Lenum === girilenBarkod) {
				if (array[i].Letyp === "TAP") {
        
				    oViewModel.setProperty("/TapKontrol", true);
				} else {
        
				    oViewModel.setProperty("/TapKontrol", false);
        
				}
			        }
        
			    }
        
			} else if (sButton === MessageBox.Action.CANCEL) {
        
			} else if (sButton === "Custom Button") {
        
			};
		        }
		    });
        
        
        
		} else if (array[i].Lenum === girilenBarkod && array[i].Status === false) {
		    var that = this,
		        inputDepoadres = oViewModel.getProperty("/DepoAdres"),
		        inputBarkod = oViewModel.getProperty("/Barkod");
		    oViewModel.setProperty("/BarkodTip", array[i].Letyp);
        
		    if (array[i].Letyp === "TAP") {
        
		        var oViewModel = that.getModel("worklistView");
		        oViewModel.getProperty("/TapKontrol");
		        oViewModel.setProperty("/TapKontrol", false);
		    } else {
        
		        oViewModel.setProperty("/TapKontrol", true);
		    }
		    oDataModel.read(
		        "/CountBarcodeSet(Lgpla='" + inputDepoadres + "',Lenum='" + inputBarkod + "')", {
			urlParameters: {
			    "$expand": "StockTab"
			},
			success: function(data) {
        
			    var oViewModel = that.getModel("worklistView");
			    oViewModel.setProperty("/StokTablosuGrupsuz", data);
			    var arrStatusFalse = data;
			    if (arrStatusFalse.Letyp === "TAP") {
        
			        var arr = arrStatusFalse.StockTab.results
			        oViewModel = that.getModel("worklistView");
			        const merged = arr.reduce((r, {
				Matnr,
				Charg,
				Bestq,
				...rest
			        }) => {
				const key = `${Matnr}-${Charg}-${Bestq}`;
				r[key] = r[key] || {
				    Matnr,
				    Charg,
				    Bestq,
				    grupArr: []
				};
				r[key]["grupArr"].push(rest)
				return r;
			        }, {})
        
			        const grupTable = Object.values(merged);
        
			        var totalCtMenge = 0;
			        for (var i = 0; i < grupTable.length; i++) {
        
				for (var j = 0; j < grupTable[i].grupArr.length; j++) {
				    totalCtMenge += parseInt(grupTable[i].grupArr[j].CtMenge);
				}
        
			        }
			        oViewModel.setProperty("/KontrolBayrak", false);
			        oViewModel.setProperty("/ToplamMenge", totalCtMenge);
			        oViewModel.setProperty("/StokTable", grupTable);
			        oViewModel.setProperty("/ExpandStok", true);
        
        
        
			        var grupTbMengeArr = [];
			        for (var i = 0; i < grupTable.length; i++) {
        
				for (var j = 0; j < grupTable[i].grupArr.length; j++) {
        
				    grupTbMengeArr.push(grupTable[i].grupArr[j]);
				}
        
			        }
			        const mergedd = grupTbMengeArr.reduce((r, {
				Lenum,
				...rest
			        }) => {
				const key = `${Lenum}`;
				r[key] = r[key] || {
				    Lenum,
				    grupArr: []
				};
				r[key]["grupArr"].push(rest)
				return r;
			        }, {})
			        const grupTable2 = Object.values(mergedd);
        
			        var sumTbMenge = 0;
			        for (var k = 0; k < grupTable2.length; k++) {
				sumTbMenge += parseInt(grupTable2[k].grupArr.length);
			        }
			        oViewModel.setProperty("/TbAdet", sumTbMenge);
        
        
        
			    } else {
			        var oViewModel = that.getModel("worklistView");
			        oViewModel.setProperty("/KontrolBayrak", true);
			        oViewModel.setProperty("/TbAdet", arrStatusFalse.TbMenge);
			        oViewModel.setProperty("/StokTable", arrStatusFalse.StockTab.results);
			        oViewModel.setProperty("/ExpandStok", true);
        
        
			    }
			},
			error: function() {
        
        
			},
		        }
		    );
		}
	        }
	    },
	    onPressTapSay: function(oEvent) {
        
	        var oViewModel = this.getModel("worklistView");
	        if (!this.newDialog) {
		this.newDialog = new sap.ui.xmlfragment("com.arete.zwmuictapp.view.Dialog", this);
		this.getView().addDependent(this.newDialog);
        
	        }
        
	        this.newDialog.open();
	    },
	    onCloseDialog: function(oEvent) {
	        this.newDialog.close();
	        var oViewModel = this.getModel("worklistView");
	        oViewModel.setProperty("/TbBarkod", "");
	        oViewModel.setProperty("/TBStokTable", []);
	        oViewModel.setProperty("/TbAdet", "");
	        oViewModel.setProperty("/StokTable", []);
	        oViewModel.setProperty("/ExpandStok", false);
        
	    },
	    onTBDialogBarkodGiris: function(oEvent) {
        
	        var oDataModel = this.getModel(),
		oViewModel = this.getModel("worklistView"),
		inputDepoadres = oViewModel.getProperty("/DepoAdres"),
		inputTBarkod = oViewModel.getProperty("/TbBarkod"),
		stokArray = oViewModel.getProperty("/StokTablosuGrupsuz"),
		tabBarkodStokArr = oViewModel.getProperty("/EklenenTabBarkodKontrol");
        
	        for (var i = 0; i < tabBarkodStokArr.length; i++) {
		for (var j = 0; j < tabBarkodStokArr[i].length; j++) {
		    if (tabBarkodStokArr[i][j].Barcode === inputTBarkod) {
        
        
		        var sText = "Okutulan TBbarkod daha önce sayıldı. Tekrar saymak istiyor musunuz?";
		        MessageBox.confirm(sText, {
			title: "Teyit",
			initialFocus: sap.m.MessageBox.Action.CANCEL,
			onClose: function(sButton) {
			    if (sButton === MessageBox.Action.OK) {
        
        
        
			    } else if (sButton === MessageBox.Action.CANCEL) {
        
			    } else if (sButton === "Custom Button") {
        
			    };
			}
		        });
        
		    }
		}
	        }
        
	        var stokTabFiltre = oViewModel.getProperty("/StokTablosuGrupsuz").StockTab.results;
	        var newArray = stokTabFiltre.filter(function(el) {
		return el.Barcode == inputTBarkod;
	        });
	        if (newArray.length) {
		debugger;
		oViewModel.setProperty("/TBStokTable", newArray);
	        } else if (!newArray.length) {
		        debugger;
		oDataModel.read(
		    "/CountBarcodeSet(Lgpla='" + inputDepoadres + "',Lenum='" + inputTBarkod + "')", {
		        urlParameters: {
			"$expand": "StockTab"
		        },
		        success: function(data) {
        
			var tabArr = data.StockTab.results;
			oViewModel.setProperty("/TBStokTable", tabArr);
		        },
		        error: function(data) {
        
        
		        },
		    }
		);
        
	        }
        
	    },
	    onPressTabEkle: function(oEvent) {
        
	        var oViewModel = this.getModel("worklistView"),
		tabBarkodStokEklenenler = oViewModel.getProperty("/EklenenTabBarkodKontrol"),
		inputTBarkod = oViewModel.getProperty("/TbBarkod"),
		tabloPush = oViewModel.getProperty("/TBStokTable");
	        for (var t = 0; t <= tabBarkodStokEklenenler.length; t++) {
		if (tabBarkodStokEklenenler[t] === inputTBarkod) {
			debugger;
		} else if (!tabBarkodStokEklenenler[t]) {
			debugger;
		    for (var t = 0; t < tabloPush.length; t++) {
		        tabBarkodStokEklenenler.push(tabloPush[t]);
        
		    }
		    break;
		}
	        }
	        oViewModel.setProperty("/Countmengetap", tabBarkodStokEklenenler.length);
	        oViewModel.setProperty("/TbBarkod", "");
	        oViewModel.setProperty("/TBStokTable", []);
        
	    },
	    onPressTabSayımTamamla: function(oEvent) {
        
	        var oViewModel = this.getModel("worklistView"),
		tabBarkodStokEklenenler = oViewModel.getProperty("/EklenenTabBarkodKontrol"),
		inputTBarkod = oViewModel.getProperty("/Barkod"),
		stokTabFiltre = oViewModel.getProperty("/StokTablosuGrupsuz").StockTab.results;
	        var aLenumFiltreArr = stokTabFiltre.filter(function(el) {
		return el.Lenum == inputTBarkod;
	        });
	        if (aLenumFiltreArr.length === tabBarkodStokEklenenler.length) {
        
		var oViewModel = this.getModel("worklistView"),
		    barkodKontrol = oViewModel.getProperty("/BarkodTable");
		for (var y = 0; y < barkodKontrol.length; y++) {
		    if (barkodKontrol[y].Lenum === inputTBarkod) {
		        var that = this,
			oViewModel = this.getModel("worklistView");
        
		        var changeStatusPath = oViewModel.getProperty("/BarkodTable/" + y + "/Status");
		        oViewModel.setProperty(("/BarkodTable/" + y + "/Status"), true);
        
		        var tabBarkodStokEklenenler = oViewModel.getProperty("/EklenenTabBarkodKontrol");
		        var anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock");
		        oViewModel.setProperty("/AnaBarkodStock", tabBarkodStokEklenenler);
        
		        var sumCount = oViewModel.getProperty("/CountMenge") + oViewModel.getProperty("/Countmengetap");
		        oViewModel.setProperty("/CountMenge", sumCount);
		        oViewModel.setProperty("/StokTable", []);
		        that.onCloseDialog();
        
        
		    } else {
        
		    }
		}
        
        
	        } else {
        
		var that = this;
		var sText = "Palete ait tüm taşıma birimleri okutulmadı, sayımı tamamlamak istiyor musunuz?";
		MessageBox.confirm(sText, {
		    title: "Teyit",
		    initialFocus: sap.m.MessageBox.Action.CANCEL,
		    onClose: function(sButton) {
		        if (sButton === MessageBox.Action.OK) {
        
			var oViewModel = that.getModel("worklistView");
			oViewModel.setProperty("/StokTable", []);
			that.onDialogCancel();
        
        
		        } else if (sButton === MessageBox.Action.CANCEL) {
        
		        } else if (sButton === "Custom Button") {
        
		        };
		    }
		});
        
        
	        }
        
        
	    },
	    onTabClose: function(oEvent) {
        
	        var that = this,
		oViewModel = this.getModel("worklistView");
	        oViewModel.setProperty("/Countmengetap", 0);
	        oViewModel.setProperty("/EklenenTabBarkodKontrol", []);
	        that.onCloseDialog();
        
	    },
	    onPressEkle: function(oEvent) {
        
	        var that = this,
		oViewModel = this.getModel("worklistView"),
		oTable = this.getView().byId("idtableStok");
	        for (var i = 0; i < oTable.getItems().length; i++) {
        
		if (!parseInt(oTable.getItems()[i].getAggregation("cells")[4].getValue())) {
        
        
		    var sText = "Sıfır sayılan stoklar var. Onaylıyor musunuz?",
		        that = this;
		    MessageBox.confirm(sText, {
		        title: "Teyit",
		        initialFocus: sap.m.MessageBox.Action.CANCEL,
		        onClose: function(sButton) {
			if (sButton === MessageBox.Action.OK) {
        
			    that.onMethodEkle();
        
			} else if (sButton === MessageBox.Action.CANCEL) {
        
			} else if (sButton === "Custom Button") {
        
			};
		        }
		    });
		} else {
        
		    that.onMethodEkle();
		}
	        }
        
        
        
	    },
	    onPressSayimiBitir: function(oEvent) {
        
	        var oDataModel = this.getModel(),
		oViewModel = this.getModel("worklistView"),
		girilenBarkod = oViewModel.getProperty("/Barkod"),
		barkodArray = oViewModel.getProperty("/BarkodTable"),
		stokArray = oViewModel.getProperty("/StokTable");
	        for (var i = 0; i < barkodArray.length; i++) {
		if (barkodArray[i].Lenum === girilenBarkod && barkodArray[i].Status === true) { //eldeki barkod var mı ve status
		    //Sayım tamamla
        
		    var anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock");
        
		    var BarkodTable = oViewModel.getProperty("/BarkodTable");
		    for (var n = 0; n < BarkodTable.length; n++) {
        
		        var oBarkod = BarkodTable[n].Lenum;
		        var aFilterAnaStok = anaBarkodStok.filter(function(el) {
			return el.Lenum == oBarkod;
		        });
        
		        var eslenenBarkod = oViewModel.getProperty("/BarkodTable")[n].StockTab;
		        eslenenBarkod = aFilterAnaStok;
		        BarkodTable[n].StockTab = eslenenBarkod
        
		    }
        
		    var depoInput = oViewModel.getProperty("/DepoAdres"),
		        that = this,
		        barkodInput = oViewModel.getProperty("/Barkod"),
		        oData = {};
		    oData.Lgpla = barkodInput;
		    oData.Ivnum = depoInput;
		    oData.BarcodeTab = BarkodTable;
		    var oDataModel = this.getModel();
		    oDataModel.create(
		        "/CountHeaderSet", oData, {
			success: function(data) {
        
			    sap.m.MessageToast.show("İşlem başarılı!");
			    var oViewModel = that.getModel("worklistView");
			    that.onMethodClear();
        
			},
			error: function() {
        
			},
		        }
		    );
		} else if (barkodArray[i].Lenum === girilenBarkod && barkodArray[i].Status === false) {
        
		    var that = this;
		    var sayımText = "Adreste sayılmayan stoklar var, sıfır stok olarak kaydedilecek. Onaylıyor musunuz?";
		    MessageBox.confirm(sayımText, {
		        title: "Teyit",
		        initialFocus: sap.m.MessageBox.Action.CANCEL,
		        onClose: function(sButton) {
			if (sButton === MessageBox.Action.OK) {
        
			    that.onSayımBitirCagir();
        
			} else if (sButton === MessageBox.Action.CANCEL) {
        
			} else if (sButton === "Custom Button") {
        
			};
		        }
		    });
		}
	        }
	    },
	    onPressBarkodClear: function(oEvent) {
	        var oViewModel = this.getModel("worklistView");
	        oViewModel.setProperty("/Barkod", "");
	        oViewModel.setProperty("/StokTable", "");
	        oViewModel.setProperty("/ExpandStok", false);
	    },
	    onMethodClear: function() {
	        var oViewModel = this.getModel("worklistView");
	        oViewModel.setProperty("/DepoAdres", "");
	        oViewModel.setProperty("/BarkodTable", []);
	        oViewModel.setProperty("/StokTable", []);
	        oViewModel.setProperty("/Barkod", "");
	        oViewModel.setProperty("/TbAdet", "");
	        oViewModel.setProperty("/Ivnum", "");
	        oViewModel.setProperty("/TotalMenge", "");
	        oViewModel.setProperty("/CountMenge", "");
	        oViewModel.setProperty("/TapKontrol", true);
	        oViewModel.setProperty("/ExpandBarkod", false);
	        oViewModel.setProperty("/BarkodTip", "");
	        oViewModel.setProperty("/ExpandStok", false);
        
        
        
        
	    },
	    onUpdateFinished: function(oEvent) {
	        // update the worklist's object counter after the table update
	        var sTitle,
		oTable = oEvent.getSource(),
		iTotalItems = oEvent.getParameter("total");
	        // only update the counter if the length is final and
	        // the table is not empty
	        if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
		sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
	        } else {
		sTitle = this.getResourceBundle().getText("worklistTableTitle");
	        }
	        this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
	    },
	    onUpdateFinishedBarkod: function(oEvent) {
	        var sTitle,
		oTable = oEvent.getSource(),
		iTotalItems = oEvent.getParameter("total");
	        // only update the counter if the length is final and
	        // the table is not empty
	        if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
		sTitle = this.getResourceBundle().getText("worklistTableTitleCountBarkod", [iTotalItems]);
	        } else {
		sTitle = this.getResourceBundle().getText("worklistTableTitleBarkod");
	        }
	        this.getModel("worklistView").setProperty("/worklistTableTitleBarkod", sTitle);
	    },
	    onSayımBitirCagir: function() {
        
	    },
	    onDialogCancel: function() {
        
	        var that = this,
		oViewModel = this.getModel("worklistView"),
		inputTBarkod = oViewModel.getProperty("/Barkod"),
		barkodKontrol = oViewModel.getProperty("/BarkodTable");
	        for (var y = 0; y < barkodKontrol.length; y++) {
		if (barkodKontrol[y].Lenum === inputTBarkod) {
        
		    var oViewModel = this.getModel("worklistView"),
		        changeStatusPath = oViewModel.getProperty("/BarkodTable/" + y + "/Status");
		    oViewModel.setProperty(("/BarkodTable/" + y + "/Status"), true);
		    var tabBarkodStokEklenenler = oViewModel.getProperty("/EklenenTabBarkodKontrol"),
		        anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock");
		    for (var n = 0; n < tabBarkodStokEklenenler.length; n++) {
		        anaBarkodStok.push(tabBarkodStokEklenenler[n]);
		    }
		    var sumCount = oViewModel.getProperty("/CountMenge") + oViewModel.getProperty("/Countmengetap");
		    oViewModel.setProperty("/CountMenge", sumCount);
		    that.onCloseDialog();
        
        
		} else {
        
		}
	        }
	    },
	    onMethodEkle: function() {
		    debugger;
	        var that = this,
		oViewModel = that.getModel("worklistView"),
		girilenBarkodKontrol = oViewModel.getProperty("/Barkod"),
		barkodTabloKontrol = oViewModel.getProperty("/BarkodTable");
	        for (var a = 0; a < barkodTabloKontrol.length; a++) {
		if (barkodTabloKontrol[a].Lenum === girilenBarkodKontrol) {
        
		    barkodTabloKontrol[a].Status === true;
		    var path = oViewModel.getProperty("/BarkodTable/" + a + "/Status");
		    oViewModel.setProperty(("/BarkodTable/" + a + "/Status"), true);
			debugger;
		} else if (!barkodTabloKontrol[a].Lenum) {
			debugger;
		    var anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock");
		    anaBarkodStok.push({
		        Lenum: girilenBarkod,
		        Status: true
		    });
		    var totalMengeGuncel = oViewModel.getProperty("/TotalMenge") + 1;
		    oViewModel.setProperty("/TotalMenge", totalMengeGuncel);
		}
	        }
	        var sayacSayılan = oViewModel.getProperty("/CountMenge") + 1;
	        oViewModel.setProperty("/CountMenge", sayacSayılan);
	        var barkodTabloKontrol = oViewModel.getProperty("/BarkodTable");
	        for (var m = 0; m < barkodTabloKontrol.length; m++) {
		if (barkodTabloKontrol[m].Letyp === "TAP") {
		    var oViewModel = that.getModel("worklistView"),
		        kenardaStokTab = oViewModel.getProperty("/StokTablosuGrupsuz").StockTab.results;
		    const mergeddd = kenardaStokTab.reduce((r, {
		        Matnr,
		        Charg,
		        Bestq,
		        ...rest
		    }) => {
		        const key = `${Matnr}-${Charg}-${Bestq}`;
		        r[key] = r[key] || {
			Matnr,
			Charg,
			Bestq,
			grupArr: []
		        };
		        r[key]["grupArr"].push(rest)
		        return r;
		    }, {})
        
		    const grupTable3 = Object.values(mergeddd);
		    var anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock");
		    anaBarkodStok.push(grupTable3);
        
        
		} else {
        
		    var oViewModel = that.getModel("worklistView"),
		        that = this,
		        anaBarkodStok = oViewModel.getProperty("/AnaBarkodStock"),
		        tempArr = oViewModel.getProperty("/StokTablosuGrupsuz");
		    for (var t = 0; t < tempArr.StockTab.results.length; t++) {
		        anaBarkodStok.push(tempArr.StockTab.results[t]);
		    }
		    oViewModel.setProperty("/StokTable", []);
		    break;
		}
	        }
        
	    }
        
        
        
        
	});
        });